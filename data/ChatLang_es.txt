﻿#
# Traducido Español by BMШ-ΒΔDBΘΨ-X
#
100|desde
101|encendido
102|Tu haz sido silenciado
103|Se te a permitido hablar de nuevo
104|Bienvenido a mi sala
105|Diviertete!
106|esto es un ejemplo de motd
107|edita texto, guardalo y usa el comando /cargarmotd
108|Sala de chat
109|Estadisticas
110|Direccion del servidor
111|Servidor comenzado en
112|Tiempo en linea
113|Conecciones aceptadas
114|Mensages recividos
115|Cuenta maxima de usuarios
116|Cuenta actual de usuarios
117|mensages enviados
118|usuarios desconectados por flood
119|Usuarios sacados
120|Usuarios
121|archivos
122|silenciados
123|Visto por ultima ves
124|Desconocido
125|Intento por desbanear fallido, usuario
126|no esta baneado
127|Desbanear rango
128|no encontrado
129|Banear rango
130|Ya esta baneado
131|Intendo de banear fallido, usuario
132|Usuario
133|Baneado
134|usuarios baneados
135|Rango
136|rangos baneados
137|No se te permite hablar estas silenciado
138|no ignorado
139|No se puede ignorar
140|demasiados usuarios ignorados
141|ya esta ignorado
142|ignorado
143|Bienvenido a la 
144|Sala
145|Permiso denegado
146|Comando desconocido
147|ejecutado
148|OpType
149|nada que cambiar
150|argumento faltante
151|no esta en la lista
152|ya esta en la lista
153|te expulsaron de la sala
154|Alguien a ingresado ala sala con tu nombre
155|Nombre ilegal
156|Proteccion de flood
157|Tiempo muerto de loguear
158|Tiempo muerto de pong
159|ya estas logueado
160|Tienes privilegios de Moderador en esta sala
161|escribe /Ayuda para ver lista de comandos accesibles
162|Tu ya no tienes privilegios de Moderador en esta sala
163|se a desconectado
164|Coneccion rechazada, usuario baneado
165|Coneccion rechazada, rango baneado
166|Reconectado demaciado rapido
167|Demaciados usuarios con el mismo IP
168|Estoy fuera del teclado
169|Lista de salas actualizadas
172|buscando anfitrion
173|Error de inicio de session
174|Nombre ya esta registrado
175|Clave muy corta
176|Nombre de usuario registrado con exito
177|Inicio de session exitoso
178|Nombre registrado
179|cuentas guardadas
180|Logueado
181|Usa /Registrar [clave] y /loguear [clave] para comprobar tu nombre en esta sala
182|salas
183|alguien ya esta logeado con tu nombre
184|Estoy fuera del teclado
185|Regreso de un estado de reposo (tiempo de reposo
186|entrado
187|Entrando
188|Estas en una sala virtual
189|a sido añadido a vespia
190|a sido removido de vespia
191|No se puede ejecutar VBloquear, tu no estas en sala virtual
192|Te invita a sala virtual, para ingresar sala virtual escribe /Vsala
193|para regresar escribe /Vsala 1
194|Mirar sala principal
195|No puedes ejecutar VLogMain, tu no estas en sala virtual
196|No puedes ejecutar VInvitar, tu no estas en sala virtual
197|Tu tienes privilegios de Administrador en esta sala
198|Tu tienes privilegios de Anfitrion en esta sala
199|Tu ya no tienes privilegios de Administrador en esta sala
200|Tu ya no tienes privilegios de Anfitrion en esta sala
201|Usuario
202|Moderator
203|Administrador
204|Anfitrion
205|Reiniciando servidor...
206|tiempo acabado
207|compartiendo
208|archivos
209|No puedes VAlias, tu no estas en sala virtual
210|error: alias ya asignado a otra sala virtual
211|error: nombre coloriado debe contener tu nombre original
212|Tu nombre a sido cambiado a
213|pedido de cliente
214|Servidores disponibles
215|de
216|servidores cargados
217|Fin de la busqueda
218|Buscando a
219|Registros limpiados
220|registros
221|deslogeado
222|session iniciada
223|Nivel Moderador
224|Nivel Administrador
225|Nivel Anfitrion
229|[usuario]
233|no encontrado, añadiendo ejemplo mensage de motd
234|Leyendo motd.txt...
236|Lineas cargadas
237|---------
# tipo de log
238|Flood
239|Banear
240|Silenciar
241|Sacados
242|Anuncio
243|Esconder
244|Pintar
245|Topic
246|Url
247|Filtro
248|Motd
249|Op
250|AcceptBanear
251|ListaServidores
252|Net
253|Vroom
254|Cuenta
255|Redireccionar
# ----------
256|Escondido
257|Vroom
258|Error pong
259|Pong
260|from
261|enviado a
263|user offline
264|[mensaje]
267|search text
268|version cliente
269|comandos
270|/Ayudaservidor
# commands
226|Matarcuenta
228|Esconder
230|Agregarlinea
231|Guardarmotd
232|Limpiarmotd
235|Cargarmotd
262|Susurrar
265|Clonar
266|Google
271|Ayuda
272|Idle
273|loguear
274|Desloguear
275|Registrar
276|Ropa
277|Ropageneral
278|Pintar
279|Despintar
280|Vsala
281|VAlias
282|VEspia
283|VMover
284|VBloquear
285|VDesbloquear
286|VInvitar
287|VLogMain
288|VAnuncio
289|VSalir
290|VLista
291|SetLevelUsuario
292|Cambiarnombre
293|Regresarnombre
295|Dejartever
296|Version
297|Motd
298|AgarrarTopic
299|RemoverTopic
300|PonerTopic
301|PonerUrl
302|Disminuir
303|Agrandar
304|Disminuirtodo
305|Agrandartodo
306|Vermotd
307|VAnnounce
311|Esconder
312|Sacar
313|Banear
314|Desbanear
315|Listabaneados
316|Limpiarbaneados
317|Banearrango
318|Desbanearrango
319|Silenciar
320|Hablar
321|Relojsilencio
322|Listasilenciados
323|Hablartodos
324|Estadisticaservidor
325|Informacionservidor
326|Listaadmins
327|Mostrarregistro
328|Limpiarregistro
329|ListaUsuarios
330|Encontrar
331|Quienera
332|Quienes
333|QuienesIP
334|IP
335|Ping
369|ClearScreen
336|Anunciar
337|Anunciarusuario
338|PubToUser
339|Anuncioadmins
340|Redireccionar
341|Listadecuentas
342|Acabarcuenta
343|Limpiarcuentas
344|LogLevel
345|SetLogLevel
346|Apagar
347|Reiniciar
# --------------
294|exitoso
308|Cargando baneados...
309|Servidor
310|El servidor comenzo
348|razon
349|todos
350|IP Reportada
351|Servidor detenido
352|IP Local
353|Escuchando en puerto TCP
354|no logra escuchar el puerto TCP
355|TCP/UDP Cortafuegos paso prueba
356|usuarios conectados
357|TCP Cortafuegos no paso la prueba! comprobar el puerto TCP 
358|Comenzando prueba TCP en cortafuegos...
359|Archivo ausente ChatConf.txt, configuracion inicial
360|Leyendo archivo ChatConf.txt
# archivos compartidos
361|Todos
362|Audio
363|Video
364|Imagen
365|Programa
366|Documento
367|Otro
368|Ninguno
# ----------
370|Vsalas
371|Bloquiado
372|Fin de VLista
373|[nueva linea motd]
374|[razon de ban]
375|[nombrenuevo]
376|[nuevo topic]
377|[url] [texto url]
378|[ip]
379|[texto de busqueda]
380|[Rango IP]
381|[clave]
382|[numero] / [alias]
383|[ropa]
384|[ropa general]
385|[alias]
386|[ip : puerto]
387|[on/off]
388|[Tiporegistro/todos]
389|[+/-]
390|[minutos]
# filtros
391|filtro Silenciar
392|filtro Censuar
393|filtro Sacar
394|filtro Ban&Kill
395|filtro PV Censurar
396|filtro PV Sacar
397|filtro PV Banear&Sacar
398|filtro Redireccionar
399|filtro Mover
400|filtro Announce
401|filtro Pub Anuncio
402|filtro PV Anuncio
403|ingreso nombre Redireccionar
404|ingreso nombre Mover
405|ingreso nombre Sacar
406|ingreso nombre Banear
407|ingreso nombre Silenciar
408|ingreso Anuncio
409|ingreso Pub Anuncio
410|ingreso PV Anuncio
# -----------
411|filtrostexto
412|filtros cargados
413|filtrotexto Redireccion Automatica
414|filtrotexto Banear
415|Fin de Registro
416|dias
417|horas
418|minutos
419|Possible nick change for killed user
420|Html
421|[html snippet]
422|HTML
423|HtmlTo
424|[user] [html snippet]
425|doesn't support this feature


