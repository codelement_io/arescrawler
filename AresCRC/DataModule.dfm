object DataModuleZeos: TDataModuleZeos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 282
  Top = 123
  Height = 269
  Width = 531
  object ZConnection: TZConnection
    ControlsCodePage = cCP_UTF8
    AutoEncodeStrings = False
    ClientCodepage = 'utf8'
    Properties.Strings = (
      'controls_cp=CP_UTF8'
      'codepage=utf8')
    HostName = 'localhost'
    Port = 3306
    Database = 'testforares'
    User = 'user2'
    Password = '1234567890'
    Protocol = 'mysql'
    LibraryLocation = '.\libmysql50.dll'
    Left = 80
    Top = 48
  end
  object ZQuery: TZQuery
    Connection = ZConnection
    Params = <>
    Left = 80
    Top = 120
  end
end
