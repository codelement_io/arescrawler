unit FileSaver;

interface

uses LogSaverAndRegRead, Classes, SysUtils, TypInfo;

type
  TFileSaver = class (TInterfacedObject, ILogSaver)
  public
    procedure LogMessage(AMsg: String);
  end;


implementation


{ TFileSaver }

procedure TFileSaver.LogMessage(AMsg: String);
var
  LogFile: TextFile;
  FileName: String;
begin
  FileName:=ExtractFilePath(ParamStr(0))+'AresCrawlerLog_'+DateToStr(Now)+'.log';
  AssignFile(LogFile, Filename);
  try
    if FileExists(FileName) then Append(LogFile) else Rewrite(LogFile);
    Writeln(LogFile, AMsg);
  finally
    CloseFile(LogFile);
  end;
end;

initialization

  AddSaver(TFileSaver.Create);

end.
