unit DataModule;

interface

uses
  SysUtils, Classes, IniFiles, Dialogs, Forms, Windows,
  DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection, ZConnection,
  vars_Global, Helper_Search_GUI, DBUpdater, LogSaverAndRegRead, ExtCtrls;

type

  TSearchThread = class(TThread)
  private
    FSearchPanels: TList;
    FCurrentSearchPanel: Integer;
    FKeywords: TStringList;
    FKeyword: string;
    procedure GetList;
    procedure StartSearch;
    procedure StopSearch;
    procedure CheckSearchTime;
    procedure StartUp;
  public
    procedure Execute; override;
  end;

  TDataModuleZeos = class(TDataModule)
    ZConnection: TZConnection;
    ZQuery: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FNumSearchKeyword: Integer;
    FNumSearchHash: Integer;
    FSearchByHash: String;
    FSearchByKeyword: String;
    FSearchThread: TSearchThread;

    function ReadFromIni: Boolean;
    procedure GetSearchParamsFromRegistry;
  public
    function ReturnKeyword: TStringList;
    function ReturnHashLinks: TStringList;
    procedure SetHashLinkStatus(const AHash: string; AStatus: Integer);
  end;

var
  DataModuleZeos: TDataModuleZeos;

implementation

uses ufrmmain;

{$R *.dfm}

type

TSearchPanel = class
  Panel: TPanel;
  StartTime: Cardinal;
  constructor Create(APanel: TPanel; AStartTime: Cardinal);
end;


procedure TDataModuleZeos.GetSearchParamsFromRegistry;
begin
  FSearchByHash:=GetFromRegistry(RegSearchByHash);
  FSearchByKeyword:=GetFromRegistry(RegSearchByKeyword);
  FNumSearchHash:=StrToIntDef(GetFromRegistry(RegNumHashSearch), 10);
  FNumSearchKeyword:=StrToIntDef(GetFromRegistry(RegNumKeywordSearch), 10);
end;

procedure TDataModuleZeos.DataModuleCreate(Sender: TObject);
begin
  GetSearchParamsFromRegistry;
  if not ReadFromIni then begin
    LogMessage(Error, 'Some error in read MySQLSettings.ini');
    Exit;
  end;

  try
    ZConnection.Connect;
  except
    on E: Exception do
        LogMessage(Error, E.Message);
  end;
  if ZConnection.Connected then
     DBUpdate(ZConnection);
  FSearchThread := TSearchThread.Create(false);
  HashLinkList:=ReturnHashLinks;
end;

function TDataModuleZeos.ReturnKeyword: TStringList;
begin
  Result := TStringList.Create;
  if not ZConnection.Connected then
     exit;
  ZQuery.SQL.Text:='select keyword from '+ZConnection.Database+'.ares_keywords';
  try
    ZQuery.Open;
    while not ZQuery.Eof do
    begin
      Result.Add(ZQuery.FieldByName('keyword').AsString);
      ZQuery.Next;
    end;
    ZQuery.Close;
  except
    on E: Exception do
        LogMessage(Error, E.Message);
  end;
end;

function TDataModuleZeos.ReadFromIni: Boolean;
var
  Ini: TIniFile;
  Str: string;
begin
  Result:=False;
  Str:=ExtractFilePath(ParamStr(0))+'MySQLSettings.ini';
  if not FileExists(Str) then begin
    LogMessage(Error, 'File MySQLSettings.ini not found');
    Exit;
  end;
  Ini:=TIniFile.Create(Str);
  try
    Str:=Ini.ReadString('ConnectionSettings', 'Hostname', '');
    if Str<>'' then ZConnection.HostName:=Str else begin
      LogMessage(Error, 'In MySQLSettings.ini not found value Hostname');
      Exit;
    end;

    Str:=Ini.ReadString('ConnectionSettings', 'Port', '');
    if Str<>'' then ZConnection.Port:=StrToIntDef(Str, 3306) else begin
      LogMessage(Error, 'In MySQLSettings.ini not fount value Port');
      Exit;
    end;

    Str:=Ini.ReadString('ConnectionSettings', 'User', '');
    if Str<>'' then ZConnection.User:=Str else begin
      LogMessage(Error, 'In MySQLSettings.ini not found value User');
      Exit;
    end;

    Str:=Ini.ReadString('ConnectionSettings', 'Password', '');
    if Str<>'' then ZConnection.Password:=Str else begin
      LogMessage(Error, 'In MySQLSettings.ini not found value Password');
    end;

    Str:=Ini.ReadString('ConnectionSettings', 'Database', '');
    if Str<>'' then ZConnection.Database:=Str else begin
      LogMessage(Error, 'In MySQLSettings.ini not found value Database');
      Exit;
    end;

    Result:=true;
  finally
    FreeAndNil(Ini);
  end;
end;

function TDataModuleZeos.ReturnHashLinks: TStringList;
begin
  Result := TStringList.Create;
  if not ZConnection.Connected then Exit;
  
  ZQuery.SQL.Text:='select hash_link from '+ZConnection.Database+'.search_hash where status=0 order by hash_link';
  try
    ZQuery.Open;
    while not ZQuery.Eof do
    begin
      Result.Add(ZQuery.FieldByName('hash_link').AsString);
      ZQuery.Next;
    end;
    ZQuery.Close;
  except
    on E: Exception do
        LogMessage(Error, E.Message);
  end;
end;

procedure TDataModuleZeos.SetHashLinkStatus(const AHash: String; AStatus: Integer);
begin
  try
    ZQuery.SQL.Text:='update '+ZConnection.Database+'.search_hash set status = :Status where hash_link = :Hash';
    ZQuery.ParamByName('Status').AsInteger:=AStatus;
    ZQuery.ParamByName('Hash').AsString:=AHash;
    ZQuery.ExecSQL;
  except
    on E: Exception do
        LogMessage(Error, E.Message);
  end;
end;

{ TSearchThread }

procedure TSearchThread.CheckSearchTime;
var
  i: Integer;
  LTickCount: Cardinal;
begin
  LTickCount := GTimeForSearch * 1000;

  for i := FSearchPanels.Count - 1 downto 0 do
    if (GetTickCount - TSearchPanel(FSearchPanels[i]).StartTime) > LTickCount then
    begin
       FCurrentSearchPanel := i;
       Synchronize(StopSearch)
    end;
end;

procedure TSearchThread.Execute;
var
  i: Integer;
begin
  Exit;
  StartUp;
  while (not Terminated) do begin
    Synchronize(GetList);
    while (not Terminated) and (FKeywords.Count > 0) do
    begin
      if ares_frmmain.pagesrc.PanelsCount <= GSearchAmount then
      begin
        FKeyword := FKeywords[0];
        Synchronize(StartSearch);
        FKeywords.Delete(0);
      end;
      Sleep(100);
      CheckSearchTime;
    end;
    FreeAndNil(FKeywords);
  end;
end;

procedure TSearchThread.GetList;
begin
  FKeywords := DataModuleZeos.ReturnKeyword;
end;

procedure TDataModuleZeos.DataModuleDestroy(Sender: TObject);
begin
  FSearchThread.Terminate
end;

procedure TSearchThread.StartSearch;
begin
  ares_frmmain.pagesrc.activepage := 0;
  ares_frmmain.combo_search.text := FKeyword;
  gui_start_search;
  FSearchPanels.Add(TSearchPanel.Create(Ares_frmMain.pagesrc.ActivePanel, GetTickCount));
end;

procedure TSearchThread.StartUp;
begin
  FSearchPanels := TList.Create;
  Sleep(1000);
end;

procedure TSearchThread.StopSearch;
begin
  ares_frmmain.pagesrc.ActivePanel := TSearchPanel(FSearchPanels[FCurrentSearchPanel]).Panel;
  gui_stop_search;
  ares_frmmain.pagesrc.DeletePanel(ares_frmmain.pagesrc.activePage);
  TSearchPanel(FSearchPanels[FCurrentSearchPanel]).Free;
  FSearchPanels.Delete(FCurrentSearchPanel);
end;

{ TSearchPanel }

constructor TSearchPanel.Create(APanel: TPanel;
  AStartTime: Cardinal);
begin
  inherited Create;
  Panel := APanel;
  StartTime := AStartTime;
end;

end.
