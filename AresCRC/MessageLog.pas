unit MessageLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, LogSaverAndRegRead, StdCtrls, Grids, ValEdit, ComCtrls;

type
  TMessageLogFrame = class(TFrame, ILogSaver)
    Memo: TRichEdit;
  private
    { Private declarations }
    procedure LogMessage(AMsg: String);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.dfm}

{ TMessageLogFrame }

constructor TMessageLogFrame.Create(AOwner: TComponent);
begin
  inherited;
  AddSaver(Self);
end;

procedure TMessageLogFrame.LogMessage(AMsg: String);
begin
  Memo.Lines.Insert(0, AMsg);
end;

end.
