unit LogSaverAndRegRead;

interface

uses Forms, Classes, TypInfo, Dialogs, Registry, SysUtils, GeoIP;

type
  TLogLevel = (Info, Warning, Error, Verbose);
  TRegSearch = (RegNumKeywordSearch, RegNumHashSearch, RegLogLevelName, RegSearchByKeyword, RegSearchByHash);

  ILogSaver = interface
    procedure LogMessage(AMsg: String);
  end;

  TShowMessageSaver = class (TInterfacedObject, ILogSaver)
  public
    procedure LogMessage(AMsg: String);
  end;

  function GetFromRegistry(AType: TRegSearch): String;
  function GetLogLevelFromRegistry: TLogLevel;
  procedure GetGeolocation(AIP: String);

  procedure LogMessage(ALevel: TLogLevel; AMsg: String); //���� ������� ����� �������, �� ������������ �� ������ ������� ������� ������� ��� �������� � ����������� ������. � ��� � uses �������� � ���������.
  procedure AddSaver(ASaver: ILogSaver);

const
  cRPath = 'Software\CRC\Ares Crawler';

implementation

  type                //������� �� ������ ������� ������
  TLogger = class
    private
     FSaverList: TInterfaceList;
     FLogLevel: TLogLevel;
    public
     Constructor Create(ALogLevel: TLogLevel);
     Destructor Destroy; override;

     procedure AddSaver(ASaver: ILogSaver);
     procedure LogMessage(ALevel: TLogLevel; AMsg: String);
  end;

var    //������� �� ������ ������� ����������
  Logger: TLogger;

//-----------------------------------------------------------------

procedure LogMessage(ALevel: TLogLevel; AMsg: String);
begin
  Logger.LogMessage(ALevel, AMsg);
end;

procedure AddSaver(ASaver: ILogSaver);
begin
  Logger.AddSaver(ASaver);
end;

procedure GetGeolocation(AIP: String);
  var
  GeoIP: TGeoIP;
  CityStr: String;
  GeoRes: TGeoIPResult;
  GeoCity: TGeoIPCity;
begin
  {GeoIP:=TGeoIP.Create('GeoLiteCity.dat');   //������ ���� ������ ������ � ����� � ���������. ����� �� ������������ �� ������ �� ��������? ���
  GeoRes:=GeoIP.GetCity('37.139.170.132', GeoCity);   //��� ���� �������������������� ��� ���������� � ������ ������
  case GeoRes of
    GEOIP_SUCCESS: Memo1.Lines.Add('succes: '+GeoCity.CountryName+' '+GeoCity.City);
    GEOIP_NODATA: memo1.Lines.Add('NODATA');
    GEOIP_ERROR_IPADDR: memo1.Lines.Add('ERROR_IPADDR');
    GEOIP_ERROR_DBTYPE: memo1.Lines.Add('ERROR_DBTYPE');
    GEOIP_ERROR_IO: memo1.Lines.Add('ERROR_IO');
  end;   }  
end;

function GetFromRegistry(AType: TRegSearch): String;
var
  Key: String;
  R: TRegistry;
begin
  case AType of
    RegNumKeywordSearch: Key:='numKeywordSearches';
    RegNumHashSearch: Key:='numHashSearches';
    RegLogLevelName: Key:='errorSeverityLevel';
    RegSearchByKeyword: Key:='searchByKeyword';
    RegSearchByHash: Key:='searchByHash';
  end;
  Result:='';
  R:=TRegistry.Create;
  try
    if R.KeyExists(cRPath) then begin
      R.OpenKeyReadOnly(cRPath);
      if R.ValueExists(Key) then Result:=R.ReadString(Key);
    end;
  finally
    FreeAndNil(R);
  end;
end;

function GetLogLevelFromRegistry: TLogLevel;
var
  Str: String;
  Int: Integer;
begin
  Str:=GetFromRegistry(RegLogLevelName);
  Int:=GetEnumValue(TypeInfo(TLogLevel), Str);
  if Int>=0 then Result:=TLogLevel(Int) else Result:=Error; //Error is default value when something wrong
end;

{ TShowMessageSaver }

procedure TShowMessageSaver.LogMessage(AMsg: String);
begin
  ShowMessage(AMsg);
end;

 {TLogger}

procedure TLogger.LogMessage(ALevel: TLogLevel; AMsg: String);
var
  i: Integer;
begin
  if ALevel<=FLogLevel then
    for i:=0 to FSaverList.Count-1 do
      ILogSaver(FSaverList[i]).LogMessage(DateTimeToStr(Now)+', '+GetEnumName(TypeInfo(TLogLevel), Ord(ALevel))+', '+AMsg);
end;

procedure TLogger.AddSaver(ASaver: ILogSaver);
begin
  FSaverList.Add(ASaver);
end;

constructor TLogger.Create(ALogLevel: TLogLevel);
begin
  FLogLevel:=ALogLevel;
  FSaverList:=TInterfaceList.Create;
end;

destructor TLogger.Destroy;
begin
  FreeAndNil(FSaverList);
  inherited;
end;



initialization
  Logger:=TLogger.Create(GetLogLevelFromRegistry);

finalization
  Logger.Destroy;

end.
