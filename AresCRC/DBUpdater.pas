unit DBUpdater;

interface

uses
  ZAbstractConnection, ZConnection, SysUtils, Dialogs, LogSaverAndRegRead, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset;

const
  DBSchemaNumber = 2;

function DBUpdate(ACon: TZConnection): Boolean;

implementation


function RunScript(AConnection: TZConnection; const AScript: string): boolean;
var
  ZQuery : TZQuery;
begin
  Result:=true;
  ZQuery:=TZQuery.Create(nil);
  try
    ZQuery.Connection := AConnection;

    ZQuery.SQL.Text := AScript;
    try
      ZQuery.ExecSQL;
    except
      Result:=False;
    end;
  finally
    FreeAndNil(ZQuery);
  end;
end;

function GetCurSchemaNum(AConnection: TZConnection): Integer;
var
  ZQuery: TZQuery;
  LDB: String;
begin
  Result := 0;
  LDB:=AConnection.Database;
  ZQuery := TZQuery.Create(nil);
  try
    ZQuery.Connection := AConnection;

    ZQuery.SQL.Text:='SHOW TABLES FROM '+LDB+' like '+QuotedStr('system');
    try
      ZQuery.Open;
      if ZQuery.IsEmpty then begin //if table 'system' don't exists then create table
        ZQuery.SQL.Text:='create table '+LDB+'.system (SchemaNumber int(10) DEFAULT 0)';
        ZQuery.ExecSQL;
        ZQuery.SQL.Text:='insert into '+LDB+'.system (SchemaNumber) values (0)';
        ZQuery.ExecSQL;
      end;
    except
      on E: Exception do
        LogMessage(Error, E.Message);
    end;

    ZQuery.SQL.Text:='show columns FROM '+LDB+'.system where Field = '+QuotedStr('SchemaNumber');
    try
      ZQuery.Open;
      if ZQuery.IsEmpty then begin //if column SchemaNumber don't exists then add column and insert
         ZQuery.SQL.Text:='ALTER TABLE '+LDB+'.system ADD COLUMN SchemaNumber int(10) DEFAULT 0';
         ZQuery.ExecSQL;
         ZQuery.SQL.Text:='insert into '+LDB+'.system (SchemaNumber) values (0)';
         ZQuery.ExecSQL;
      end;
    except
      on E: Exception do
        LogMessage(Error, E.Message);
    end;

    ZQuery.SQL.Text:='select SchemaNumber from '+LDB+'.system';
    try
      ZQuery.Open;
      if not ZQuery.IsEmpty then begin
        ZQuery.First;
        Result:=ZQuery.FieldByName('SchemaNumber').AsInteger;
      end else Result:=0;
    except
      on E: Exception do begin
        Result:=-1;
        LogMessage(Error, E.Message);
      end;
    end;
   finally
     FreeAndNil(ZQuery);
   end;
end;

function RunUpdate1(AConnection: TZConnection): Boolean;
var
  Script: String;
begin
  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.ares_keywords;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.ares_keywords ('+
          'keyword varchar(30) NOT NULL Default "", '+
          'Primary key (keyword)'+
          ') Engine=InnoDB Default charset=utf8;';
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.ares_dl_headers;');
  if not Result then Exit;
  
  Script:='CREATE TABLE '+AConnection.Database+'.ares_dl_headers ('+
          'ip char(15) NOT NULL, '+
          'port int(5) NOT NULL, '+
          'sha1 char(32) NOT NULL, '+
          'header_variable varchar(50) NOT NULL, '+
          'header_value varchar(250) NOT NULL, '+
          'time_added timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'+
          ') ENGINE=MyISAM DEFAULT CHARSET=utf8;';
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.badFiles');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.badFiles ('+
          'sha1 char(32) NOT NULL DEFAULT "", '+
          'category char(2) DEFAULT NULL, '+
          'PRIMARY KEY (sha1)'+
          ')'; //ENGINE=TokuDB DEFAULT CHARSET=latin1 ROW_FORMAT=TOKUDB_ZLIB;   
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Script:='insert into '+AConnection.Database+'.badFiles (sha1, category) values '+
          '("26K3FJPPX2RWDBFWBBD7XUDD7ULTZ5UW", "CN"), ("2KANTVDJBTLNDMRN4YBQOQPJYWKAWAG3", "CN"), '+
          '("2RFBRU46F4EYDHCXJ6XQBTX22KSABUT6", "CN"), ("2SPA5WJU5GZZ4EQYIQRCLNZML332JHSW", "CN"), '+
          '("2ZUZ7YZKGA4HF6ICKSLVLVHSLWKRFA4D", "CN"), ("3J237DMALL3EK22WAYUWVSZZCT272F7Y", "CN"), '+
          '("3LZMHUVZN4YSQAW2D245GNXGB5PXUBQZ", "CN"), ("5YTU4O7FELZRI66A4G2RKOZ4SPYJO2TY", "CN"), '+
          '("67VAELGNVUAJZE5MZIQ6AZ54XSLNESU4", "CN"), ("7EO2I7SSMUGR6OH32GJSJ4H2TJP3J6IB", "CN"), '+
          '("AQIZ4ZBIQBJUZDFUSIKXNYNNFU5MQLDN", "CN"), ("BI6HZ5KNKUFV47DWZBILSNRLV47AE76O", "CN"), '+
          '("BJTFTOCKOEEVYIL2ZGVHVJVMPVRVK2N6", "CN"), ("C5CHHZUXJ4FXS4ZAGOF43SFFM2U4ZGFG", "CN"), '+
          '("CG3KYDTJXANK4IXS3MYDH2T5JFE7WM43", "CN"), ("EECPIVOGRRBIAJFJD6672HAZTVBPSKHI", "CN"), '+
          '("ETM5EXK22DDSU3GB5XZ4RAVDQNGMV4T2", "CN"), ("EZMXF7L4FCZZBR2RWGN3NFSGS2L6SOCR", "CN"), '+
          '("FIRHV2HVGCHMKSP6FN7O5JEZS6E5BV3V", "CN"), ("FUNXOYXVCOOWK24NT6Q667ZUTH4ZF4UO", "CN"), '+
          '("FVCBZO3P4KPZZJ5U3VK6GLX5SNSVOCYF", "CN"), ("GCGCGUIMJVJZEOUT6UVGTNWHNYZ2YHOF", "CN"), '+
          '("GSEDDK62U2NXZHWNFT3JANHO2LKET2KL", "CN"), ("HJQRXYM2HUAGGC57T33HAED6ZZTKMDU4", "CN"), '+
          '("HREDNYPCPLFCKGLYUH2RU2REJ5DRHXGH", "CN"), ("HV2LZ4GX5DWWILE2L62LQGPIOGLUDAYR", "CN"), '+
          '("INIXQD7YQA5L4BHPBYV27DTVFNMPR4YR", "CN"), ("JFFK2EV2C4JHBXLM2TXWKX5U575O54SW", "CN"), '+
          '("KWZTGHGANVQVX5NYBE4ZOUWNPLBW3HDL", "CN"), ("L25HKRQ7VL7MW3SNCPW53MRBBCHCXWSD", "CN"), '+
          '("LA643UMKPSDWXC3CTHQNYEEYFNI3DE35", "CN"), ("LO2Q4I2RP3SHBMEDI2XOU5GQW52GXEBT", "CN"), '+
          '("M7HULBIDQHHVTWKFV7ELLP54SAMBQAC3", "CN"), ("MNES36RN7OAXTGKQ3Q2MHZB5I5AGHOGL", "CN"), '+
          '("NN4NB5YFBALC32R5DHUYAQPU4QBNTBAJ", "CN"), ("O5KLG2SZECKEPB34SZJ2BJA72CZG4422", "CN"), '+
          '("OVM5HCCPOEFG244PYQ2PBEU7LI2H7XBD", "CN"), ("OZVX2JR5HXHPWY2AGOGSYN7523S3MHY3", "CN"), '+
          '("REKMHDHK5UGCP77J3RNMDWTCOLXZZSLD", "CN"), ("REUCHJ4LVRTLXSGF4Z3XR6WP3PVCV5P5", "CN"), '+
          '("RVHXV4GSTSTYLFGD662B62DGNYGTROLI", "CN"), ("UFVY36LA73WENRI535UM35FJC6V2ZOLT", "CN"), '+
          '("UQ22PMJ26X4IWBJLWRHASN5X66NG4LQG", "CN"), ("VA6VB7W227PNZDGSMBZ6P5A7LU4TXN6N", "CN"), '+
          '("VUCJVPRKS7EE52627CHM5NN4TTI3UVY5", "CN"), ("VX3R2UYU3OEM2HDSH76NYBICOMR2YU5A", "CN"), '+
          '("X65CJJNTQQHWEQIF47XG3WIG6IBU7RL4", "CN"), ("XMPDM7ASON5VMH4U3Z5FWC7TTOH5APO7", "CN"), '+
          '("YEVB4YWEFUK7HKXEC3ALBUZOK6ACGNJV", "CN"), ("YSHPO4ZKG4JNEYU5MM7CSLZKGOH4XZ63", "CN");';
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.bad_ips;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.bad_ips ('+
          'ip char(15) NOT NULL, '+
          'country char(2) NOT NULL, '+
          'time_added timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '+
          'contacted_first timestamp NOT NULL DEFAULT "0000-00-00 00:00:00", '+
          'contacted_last timestamp NOT NULL DEFAULT "0000-00-00 00:00:00", '+
          'PRIMARY KEY (ip), '+
          'KEY ByCountry (country)'+
          ')'; //ENGINE=TokuDB DEFAULT CHARSET=utf8 ROW_FORMAT=TOKUDB_ZLIB;  //ENGINE=InnoDB DEFAULT CHARSET=utf8;
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Script:='insert into '+AConnection.Database+'.bad_ips(ip, country, time_added, contacted_first, contacted_last) values '+
          '("1.2.3.4", "US", "2018-05-07 12:11:33", "2018-05-07 12:13:33", "2018-05-08 12:11:33"), '+
          '("2.3.4.5", "GB", "2018-06-07 15:19:45", "0000-00-00 00:00:00", "0000-00-00 00:00:00");';
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.candidates;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.candidates ('+
          'id int(10) NOT NULL AUTO_INCREMENT, '+
          'ip char(15) CHARACTER SET latin1 NOT NULL, '+
          'port int(5) NOT NULL, '+
          'sha1 char(32) CHARACTER SET latin1 NOT NULL, '+
          'filename varchar(250) NOT NULL DEFAULT "", '+
          'filesize bigint(11) NOT NULL, '+
          'country char(2) CHARACTER SET latin1 NOT NULL, '+
          'time_added timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '+
          'time_contacted timestamp NOT NULL DEFAULT "0000-00-00 00:00:00", '+
          'verifier_ip char(15) CHARACTER SET latin1 NOT NULL DEFAULT "", '+
          'verifier_port int(5) NOT NULL DEFAULT -1, '+
          'verifier_time_attempted timestamp NOT NULL DEFAULT "0000-00-00 00:00:00", '+
          'software_client varchar(40) CHARACTER SET latin1 NOT NULL DEFAULT "", '+
          'username varchar(32) NOT NULL DEFAULT "", '+
          'UNIQUE KEY ip (ip, port, sha1), '+
          'UNIQUE KEY id (id), '+
          'KEY byTsIp (ip), '+
          'KEY byIpTs (ip), '+
          'KEY byTimeContacted (time_contacted), '+
          'KEY byCountryIp (country, ip) '+
          ') ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;';
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.hostdata;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.hostdata ('+
          'username varchar(32) NOT NULL COMMENT "unique host ID", '+
          'filename varchar(255) NOT NULL, '+
          'fileext varchar(10) DEFAULT NULL COMMENT "filename", '+
          'sha1 char(32) NOT NULL COMMENT "unique file ID", '+
          'crawl_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '+
          'ip char(15) NOT NULL, '+
          'port int(5) NOT NULL, '+
          'filesize bigint(11) NOT NULL, '+
          'batch char(6) NOT NULL, '+
          'query varchar(20) DEFAULT "", '+
          'country char(2) DEFAULT NULL, '+
          'category char(2) DEFAULT NULL, '+
          'crawlerIP char(15) DEFAULT NULL COMMENT "ip of program making the contact", '+
          'crawlerPort int(5) DEFAULT NULL COMMENT "port of program making the contact", '+
          'PRIMARY KEY (username, sha1, ip, port, batch), '+
          'KEY byGUID (username), '+
          'KEY byTS (crawl_time), '+
          'KEY byIpGuidTS (ip, username, crawl_time), '+
          'KEY byGuidIpTS (username, ip, crawl_time), '+
          'KEY byIPHash (ip, sha1), '+
          'KEY byIP (ip, crawl_time), '+
          'KEY bySHA1 (sha1, crawl_time), '+
          'KEY country (country, ip, crawl_time)'+
          ');';   //ENGINE=TokuDB DEFAULT CHARSET=latin1 ROW_FORMAT=TOKUDB_ZLIB;  //ENGINE=InnoDB DEFAULT CHARSET=latin1
  Result:=RunScript(AConnection, Script);
  if not Result then Exit;

  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.verified_hostdata;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.verified_hostdata ('+
          'username varchar(32) NOT NULL COMMENT "unique host ID", '+
          'filename varchar(255) NOT NULL, '+
          'fileext varchar(10) DEFAULT NULL, '+
          'sha1 char(32) NOT NULL COMMENT "unique file ID", '+
          'crawl_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '+
          'epoch int(13) unsigned NOT NULL, '+
          'ip char(15) NOT NULL, '+
          'port int(5) NOT NULL, '+
          'real_ip char(15) NOT NULL, '+
          'real_port int(5) NOT NULL, '+
          'filesize bigint(11) NOT NULL, '+
          'batch char(8) NOT NULL DEFAULT "" COMMENT "YYYYMMDD", '+
          'country char(2) NOT NULL, '+
          'category char(2) NOT NULL, '+
          'software_client varchar(30) NOT NULL, '+
          'crawler_ip char(15) NOT NULL DEFAULT "", '+
          'crawler_port int(5) NOT NULL, '+
          'PRIMARY KEY (sha1, ip, port, batch), '+
          'KEY byIPHash (ip, sha1), '+
          'KEY Bycrawl_time (crawl_time), '+
          'KEY country (country, ip, crawl_time), '+
          'KEY bySHA1 (sha1, crawl_time)'+
          '); '; //ENGINE=TokuDB DEFAULT CHARSET=latin1 ROW_FORMAT=TOKUDB_ZLIB;  //ENGINE=InnoDB DEFAULT CHARSET=latin1
  Result:=RunScript(AConnection, Script);
end;

function RunUpdate2(AConnection: TZConnection): Boolean;
var
  Script: String;
begin
  Result:=RunScript(AConnection, 'DROP TABLE IF EXISTS '+AConnection.Database+'.search_hash;');
  if not Result then Exit;

  Script:='CREATE TABLE '+AConnection.Database+'.search_hash ('+
          'id int(10) NOT NULL AUTO_INCREMENT, '+
          'hash_link char(32) NOT NULL DEFAULT "", '+    //sha1
          'status int(10) NOT NULL DEFAULT 0, '+
          'PRIMARY KEY (id)'+
          ')';
  Result:=RunScript(AConnection, Script);
end;

function DBUpdate(ACon: TZConnection): Boolean;
var
  CurSchemaNumber, UpdateSchemaNumber: Integer;
  ZQuery: TZQuery;
begin
  Result:=False;
  CurSchemaNumber:=GetCurSchemaNum(ACon);
  if CurSchemaNumber=-1 then begin //can't get schemanumber
    LogMessage(Error, 'Cant get SchemaNumber from DB');
    Exit;  
  end;
  
  if CurSchemaNumber<DBschemaNumber then begin
    UpdateSchemaNumber:=CurSchemaNumber;
    ZQuery:=TZQuery.Create(nil);
    ZQuery.Connection:=ACon;
    try
      if CurSchemaNumber<1 then begin            
        if not RunUpdate1(ACon) then Exit;
        UpdateSchemaNumber := 1;
      end;
      
      if CurSchemaNumber<2 then begin
        if not RunUpdate2(ACon) then Exit;
        UpdateSchemaNumber := 2;
      end;

      Result:=true; //when all good
    finally
      if UpdateSchemaNumber<>CurSchemaNumber then begin
        ZQuery.SQL.Text := 'update '+ACon.Database+'.system SET SchemaNumber = ' + IntToStr(UpdateSchemaNumber);  //update not working if table haven't records
        ZQuery.ExecSQL;
      end;
      FreeAndNil(ZQuery);
    end;
  end else Result:=True; //when not need to update
end;

end.
