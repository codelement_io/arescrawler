unit uDBUpdate;

interface

uses
  Forms, SysUtils, Dialogs, Controls, uDM1, ULoadForm, _frmBackup, ShlObj, Windows, MyAccess;

const
  DBSchemaNumber = 34;
  CSIDL_PROGRAM_FILES = $0026; { C:\Program Files }
  C_MySQL_AllsystemsMax = 'MySQL_AllsystemsMax';

function CheckSchemaNumber(CurForm : TFrmLoadForm; AConnection: TMyConnection) : boolean;

implementation

uses
  DAScript, MyScript, _consts, Umain;

function IsConnecedOnlyCurrentUserToDB(AConnection: TMyConnection): Boolean;

  function GetCountConnecredUser: Integer;
  begin
    Result := 0;

    with TMyQuery.Create(nil) do
    try
      Connection := AConnection;
      CommandTimeout := 300;
      SQL.Text := 'SHOW PROCESSLIST';
      Execute;

      First;
      while not Eof do
      begin
        if (FieldByName('db').AsString = DM1.ProgrammDataConnection.Database) or
           (FieldByName('db').AsString = AConnection.Database)
        then
          Inc(Result);

        Next;
      end;
    finally
      Free;
    end;
  end;

{var
  LCountUser: Integer;}
begin
(* 10-9-2017 by Steve: Igor says can ignore this and it is causing problems starting with 9.0.0.28 so eliminated
  repeat
    LCountUser := GetCountConnecredUser;
    Result := LCountUser = {$IFDEF VirtualUI}3{$ELSE}2{$ENDIF};
  until Result or
        (MessageBoxEx(0, PChar(C_CANNOT_UPDATE_DB_MORE_THEN_CURRENT_USER), 'Attention', MB_RETRYCANCEL, LANG_ENGLISH) = ID_CANCEL);
*)
  result:=true;
end;

function RunScript(AConnection: TMyConnection; const AScript: string): boolean;
var
  qUpdateScript : TMyQuery;
begin
  Result := true;

  qUpdateScript := TMyQuery.Create(nil);
  try
    qUpdateScript.Connection := AConnection;
    qUpdateScript.CommandTimeout := 300;

    qUpdateScript.SQL.Text := AScript;
    try
      qUpdateScript.ExecSQL;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
end;

function RunUpdate1(AConnection: TMyConnection) : boolean;
begin
  Result := true;
end;

function RunUpdate2(AConnection: TMyConnection) : boolean;
begin
  Result := true;
end;

function RunUpdate3(AConnection: TMyConnection) : boolean;
begin
  result := RunScript(AConnection, 'CREATE TABLE IF NOT EXISTS sms_history (' +
                                                                       'id INT(11) NOT NULL AUTO_INCREMENT, ' +
                                                                       'CustKey INT(11) NOT NULL, ' +
                                                                       'PhoneNumber varchar(21) NOT NULL, ' +
                                                                       'Message TEXT, ' +
                                                                       'MessageID varchar(36) NOT NULL, ' +
                                                                       'Queued BOOL, ' +
                                                                       'Cancelled BOOL, ' +
                                                                       'Sent BOOL, ' +
                                                                       'SMSError varchar(100), ' +
                                                                       'SentDateTime DATETIME, ' +
                                                                       'PRIMARY KEY (id), ' +
                                                                       'INDEX cust_ind (CustKey), ' +
	                                                               'FOREIGN KEY (CustKey) REFERENCES customer(nKey) ON DELETE CASCADE' +
                                                                      ')')
end;

function RunUpdate4(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'SET foreign_key_checks = 0;' +
                              'ALTER TABLE transaction ADD CONSTRAINT fkt_job FOREIGN KEY (kJob) REFERENCES job(nKey) ON DELETE CASCADE;' +
                              'ALTER TABLE authorization ADD CONSTRAINT fka_job FOREIGN KEY (kJob) REFERENCES job(nKey) ON DELETE CASCADE;' +
                              'SET foreign_key_checks = 1;' +
                              'UPDATE po, vvendor SET vendor = FullName WHERE po.KVendor = vvendor.nKey AND FullName <> vendor;')
end;

function RunUpdate5(AConnection: TMyConnection) : boolean; 
begin
  Result := RunScript(AConnection, 'ALTER TABLE invoice ADD ManualInputFee boolean DEFAULT 0');
end;

function RunUpdate6(AConnection: TMyConnection) : boolean;
var
  qUpdateScript, qSelectScript : TMyQuery;
begin
  Result := true;

  qUpdateScript := TMyQuery.Create(nil);
  qSelectScript := TMyQuery.Create(nil);
  try
    qUpdateScript.Connection := AConnection;
    qUpdateScript.CommandTimeout := 300;
    qSelectScript.Connection := AConnection;
    qSelectScript.CommandTimeout := 300;

    try
      qUpdateScript.SQL.Text := 'DELETE FROM vehicle WHERE Plate is NULL;' +
                                'DELETE FROM job WHERE kInvoice is NULL;' +
                                'DELETE FROM job WHERE kEstimate is NULL;' +
                                'ALTER TABLE vehicle MODIFY Plate varchar(13) NOT NULL;';
      qUpdateScript.ExecSQL;

      qSelectScript.SQL.Text := 'use INFORMATION_SCHEMA; ' +
                                'select CONSTRAINT_NAME from KEY_COLUMN_USAGE ' +
                                'where TABLE_SCHEMA = ' + QuotedStr(DM1.MyConnection.DataBase) + ' and  TABLE_NAME = ''job'' and ' +
                                'REFERENCED_COLUMN_NAME = ''nKey'' and COLUMN_NAME in (''kEstimate'', ''kInvoice'');' +
                                'use ' + AConnection.DataBase;
      qSelectScript.Execute;

      if qSelectScript.RecordCount > 0 then
        while not (qSelectScript.Eof) do
        begin
          qUpdateScript.SQL.Text := 'ALTER TABLE job DROP foreign key ' + qSelectScript.FieldByName('CONSTRAINT_NAME').AsString;
          qUpdateScript.ExecSQL;

          qSelectScript.Next;
        end;

      qUpdateScript.SQL.Text := 'ALTER TABLE job MODIFY kEstimate INT(11) NOT NULL;' +
                                'ALTER TABLE job MODIFY kInvoice INT(11) NOT NULL;' +
                                'SET foreign_key_checks = 0;' +
                                'ALTER TABLE job ADD CONSTRAINT FK_job_Estimate_nKey FOREIGN KEY (kEstimate) REFERENCES estimate(nKey);' +
                                'ALTER TABLE job ADD CONSTRAINT FK_job_Invoice_nKey FOREIGN KEY (kInvoice) REFERENCES invoice(nKey);' +
                                'SET foreign_key_checks = 1;';
      qUpdateScript.ExecSQL;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
    FreeAndNil(qSelectScript);
  end;
end;

function RunUpdate7(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE vtechnician ADD WorkstationID char(8)')
end;

function RunUpdate8(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE system ADD pix_folder_location varchar(1000);' +
                              'CREATE TABLE IF NOT EXISTS pix (' +
                                    'nKey INT(11) NOT NULL AUTO_INCREMENT, ' +
                                    'VIN varchar(20) NOT NULL, ' +
                                    'plate varchar(13), ' +
                                    'filename varchar(100), ' +
                                    'datetimestamp DATETIME, ' +
                                    'PRIMARY KEY (nKey) ' +
                              ')')
end;

function RunUpdate9(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE sms_history MODIFY CustKey INT(11);' +
                              'ALTER TABLE sms_history ADD Parent_MessageID varchar(36);' +
                              'ALTER TABLE sms_history ADD ReceiveDateTime DATETIME;')
end;

function RunUpdate10(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE system ADD SMSSignature varchar(255);')
end;

function RunUpdate11(AConnection: TMyConnection): boolean;

  function IncludeTrailingPathDelimiter(const S: string): string;
  begin
    Result := S;
    if not IsPathDelimiter(Result, length(Result)) then
      Result := Result + '\';
  end;

var
  qUpdateScript: TMyScript;
  LFileName: string;
begin
  LFileName := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'sql\program_data.sql';

  Result := FileExists(LFileName);

  if Result then
  begin
    qUpdateScript := TMyScript.Create(nil);

    try
      qUpdateScript.Connection := AConnection;

      try
        qUpdateScript.ExecuteFile(LFileName);

        // back to current DB
        qUpdateScript.SQL.Text := 'use `' + AConnection.Database + '`;';
        qUpdateScript.Execute;
      except
        on E: Exception do
        begin
          MessageDlg(E.Message, mtError, [mbOk], 0);
          Result := false;
        end;
      end;
    finally
      FreeAndNil(qUpdateScript);
    end;
  end;
end;

function RunUpdate12(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE system ' +
                              'ADD COLUMN DefaultTimeBeginRO datetime NOT NULL DEFAULT ''2000-01-01 07:00:00'', ' +
                              'ADD COLUMN DefaultTimeEndRO datetime NOT NULL DEFAULT ''2000-01-01 08:00:00'', ' +
                              'ADD COLUMN DefaultTimeBeginPlanner datetime NOT NULL DEFAULT ''2000-01-01 07:00:00'', ' +
                              'ADD COLUMN DefaultTimeEndPlanner datetime NOT NULL DEFAULT ''2000-01-01 17:00:00'';')
end;

function RunUpdate13(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE sms_history CHANGE Queued TypeMessage varchar(5) NOT NULL;' +
                              'ALTER TABLE sms_history ADD FILES TEXT;')
end;

function RunUpdate14(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE system ADD UseImageOnly boolean DEFAULT 0')
end;

function RunUpdate15(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE system ADD SMSSessionKey varchar(50)')
end;

function RunUpdate16(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'CREATE TABLE IF NOT EXISTS NexpartVendor (' +
                              'id INT NOT NULL AUTO_INCREMENT, ' +
                              'VendorName varchar(50), ' +
                              'Link varchar(1000), ' +
                              'Username varchar(50), ' +
                              'PRIMARY KEY (id))')
end;

function RunUpdate17(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE NexpartVendor DROP COLUMN Link, DROP COLUMN username, ADD VendorKey varchar(50);' +
                              'ALTER TABLE System ADD COLUMN NextPartLogin varchar(50);')
end;

function RunUpdate18(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE NexpartVendor DROP COLUMN VendorKey, ADD VendorUser varchar(50);' +
                              'ALTER TABLE System DROP COLUMN NextPartLogin;')
end;

function RunUpdate19(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'delete from inventory where itemnumber is null;' +
                              'ALTER TABLE inventory MODIFY itemnumber VARCHAR(55) not null;')
end;

function RunUpdate20(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE NexpartVendor ADD kVendor int;')
end;

function RunUpdate21(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE NexpartVendor DROP COLUMN VendorName;')
end;

function RunUpdate22(AConnection: TMyConnection) : boolean;
{SM on 8-7-2017: RunUPdate22 replaces 16-17-18-20-21}
begin
  Result := RunScript(AConnection, 'DROP TABLE IF EXISTS NexpartVendor;' +
                              'CREATE TABLE NexpartVendor (' +
                              'id INT(11) NOT NULL AUTO_INCREMENT, ' +
                              'VendorUser varchar(50) DEFAULT NULL, ' +
                              'kVendor INT(11) DEFAULT NULL, ' +
                              'PRIMARY KEY (id))')
end;

function RunUpdate23(AConnection: TMyConnection) : boolean;
begin
  Result:=true;
{ 2018-06-05: users table is now managed independently
  Result := RunScript(AConnection, 'CREATE TABLE IF NOT EXISTS Users (' +
                              'id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, ' +
                              'UserName varchar(20) NOT NULL, ' +
                              'Password varchar(20) NOT NULL, ' +
                              'dbName varchar(50) NOT NULL, ' +
                              'AccessLevel varchar(50) NOT NULL, ' +
                              'Active TINYINT(1) default 0)')
}
end;

function RunUpdate24(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE transaction DROP FOREIGN KEY FK_transaction_Inventory_nKey; ' +
                              'ALTER TABLE transaction ADD FOREIGN KEY (kInventory) REFERENCES inventory(nKey) ON DELETE NO ACTION ON UPDATE NO ACTION;' +
                              'Delete from inventory where itemnumber = '''' ')  {Steve added on 10-9-2017}
end;

function RunUpdate25(AConnection: TMyConnection) : boolean;
var
  qUpdateScript : TMyQuery;
begin
  Result := true;
{ 2018-06-05: users table is now managed independently
  qUpdateScript := TMyQuery.Create(nil);
  try
    qUpdateScript.Connection := DM1.ProgrammDataConnection;
    qUpdateScript.CommandTimeout := 300;

    qUpdateScript.SQL.Text :=
      'SELECT COUNT(1) FROM INFORMATION_SCHEMA.STATISTICS ' +
      'WHERE table_schema=''program_data'' AND table_name=''users'' AND index_name=''UK_users'';';

    qUpdateScript.Open;

    if qUpdateScript.Fields[0].AsInteger = 0 then
    begin
      qUpdateScript.SQL.Text := 'ALTER TABLE users ADD UNIQUE INDEX UK_users (UserName, Password, dbName);';

      try
        qUpdateScript.ExecSQL;
      except
        on E: Exception do
        begin
          MessageDlg(E.Message , mtError, [mbOk], 0);
          Result := false;
        end;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
}
end;

function RunUpdate26(AConnection: TMyConnection) : boolean;
var
  qUpdateScript : TMyQuery;
begin
  Result := true;

  qUpdateScript := TMyQuery.Create(nil);
  try
    qUpdateScript.Connection := DM1.ProgrammDataConnection;
    qUpdateScript.CommandTimeout := 300;

    qUpdateScript.SQL.Text :=
      'SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS ' +
      'WHERE TABLE_NAME = ''users'' AND COLUMN_NAME = ''Enabled'';';

    try
      qUpdateScript.Open;

      if qUpdateScript.Fields[0].AsInteger = 0 then
      begin
        qUpdateScript.SQL.Text :=
          'ALTER TABLE users ADD COLUMN Enabled TINYINT(1) default 1;';

        try
          qUpdateScript.ExecSQL;
        except
          on E: Exception do
          begin
            MessageDlg(E.Message , mtError, [mbOk], 0);
            Result := false;
          end;
        end;
      end;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
end;

function RunUpdate27(AConnection: TMyConnection) : boolean;
var
  qUpdateScript : TMyQuery;
begin
  Result := true;

  qUpdateScript := TMyQuery.Create(nil);

  qUpdateScript.Connection := aConnection;
  qUpdateScript.CommandTimeout := 300;

  qUpdateScript.SQL.Text :=
      'SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS ' +
      'WHERE TABLE_SCHEMA= ' + QuotedStr(AConnection.Database) + ' AND TABLE_NAME = ''system'' AND COLUMN_NAME = ''SMSLogin'';';
  try
    qUpdateScript.Open;

    if qUpdateScript.Fields[0].AsInteger = 0 then
    try
      qUpdateScript.SQL.Text := 'ALTER TABLE system ADD COLUMN SMSLogin VARCHAR(20), ' +
                                                   'ADD COLUMN SMSPassword VARCHAR(20);' +
                                'ALTER TABLE nexpartvendor ADD COLUMN UserPassword VARCHAR(20);';
      try
        qUpdateScript.ExecSQL;
      except
        on E: Exception do
        begin
          MessageDlg(E.Message , mtError, [mbOk], 0);
          Result := false;
        end;
      end;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
end;

function RunUpdate28(AConnection: TMyConnection) : boolean;
var
  qUpdateScript : TMyQuery;
begin
  Result := true;

  qUpdateScript := TMyQuery.Create(nil);
  try
    qUpdateScript.Connection := AConnection;
    qUpdateScript.CommandTimeout := 300;
    qUpdateScript.SQL.Text := 'Update system set ClinkAcct= :clinkacct';
    qUpdateScript.Params[0].asString:= '';
    try
      qUpdateScript.ExecSQL;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
end;

function RunUpdate29(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'delete from inventory where KTranType is null')
end;

function RunUpdate30(AConnection: TMyConnection) : boolean;
var
  qUpdateScript : TMyScript;
begin
  Result := true;

  qUpdateScript := TMyScript.Create(nil);
  try
    qUpdateScript.Connection := AConnection;
    qUpdateScript.SQL.Text := 'DROP FUNCTION `ConvertInteger`; ';
    try
      qUpdateScript.Execute;
    except
    end;

    qUpdateScript.SQL.Text := C_SQL_ConvInt;
    try
      qUpdateScript.Execute;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        Result := false;
      end;
    end;
  finally
    FreeAndNil(qUpdateScript);
  end;
end;

function RunUpdate31(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection, 'ALTER TABLE System ADD EnabledKUKUIService boolean DEFAULT 0')
end;

function RunUpdate32(AConnection: TMyConnection) : boolean;
var
  sScript: string;
begin
  sScript :=
    'CREATE TRIGGER Parts_PreventDeletion ' +
    'BEFORE DELETE ON Inventory ' +
    'FOR EACH ROW ' +
    'BEGIN ' +
    ' IF OLD.KTranType = 0 THEN ' +
    '   SIGNAL SQLSTATE "45000" ' +
    '   SET MESSAGE_TEXT = "The parts are not allowed to be removed";' +
    ' END IF; ' +
    'END;';
  Result := RunScript(AConnection, sScript);
end;

function RunUpdate33(AConnection: TMyConnection) : boolean;
begin
  Result := RunScript(AConnection,
    'ALTER TABLE pix ADD COLUMN jobkey INT(11) DEFAULT NULL, ' +
                    'ADD COLUMN jobstatus INT(11) DEFAULT NULL')
end;

function RunUpdate34(AConnection: TMyConnection) : boolean;
var
  sScript: string;
begin
  sScript :=
    'CREATE TABLE links (' +
      'nKey INT(11) NOT NULL AUTO_INCREMENT,' +
      'kJob INT(11) NOT NULL,' +
      'LinkAddress VARCHAR(250) NULL DEFAULT NULL,' +
      'LinkAlias VARCHAR(100) NULL DEFAULT NULL,' +
      'UseAlias TINYINT(4) NULL DEFAULT NULL,' +
      'LinkType INT(11) NULL DEFAULT NULL,' +
      'datetimestamp DATETIME NULL DEFAULT NULL,' +
      'PRIMARY KEY (nKey));' +
    'CREATE TABLE linktype (' +
      'nKey INT(11) NOT NULL AUTO_INCREMENT,' +
      'linktype VARCHAR(50) NULL DEFAULT NULL,' +
      'extension VARCHAR(250) NULL DEFAULT NULL,' +
      'PRIMARY KEY (`nKey`));' +
    'INSERT INTO linktype(linktype) VALUES(''unknown'');' +
    'INSERT INTO linktype(linktype, extension) VALUES(''image'', ''.png;.jpg;.jpeg;.bmp;'');' +
    'INSERT INTO linktype(linktype, extension) VALUES(''document'', ''.doc;.xls;.xlsx;.txt;'');' +
    'INSERT INTO linktype(linktype, extension) VALUES(''hyperlink'', ''http;https;www;'');';

  Result := RunScript(AConnection, sScript);
end;


function ProgramFilesPath: string;
begin
   SetLength(Result, MAX_PATH);
   SHGetSpecialFolderPath(0, PChar(Result), CSIDL_PROGRAM_FILES , True);
   SetLength(Result, StrLen(PChar(Result)));
end;

function CheckSchemaNumber(CurForm : TFrmLoadForm; AConnection: TMyConnection) : boolean;
var
  CurSchemaNumber, UpdateSchemaNumber : integer;
  qSchemaNumber : TMyQuery;
begin
  Result := false;
  CurSchemaNumber := 0;

  qSchemaNumber := TMyQuery.Create(nil);
  try
    qSchemaNumber.Connection := AConnection;
    qSchemaNumber.CommandTimeout := 300;

    qSchemaNumber.SQL.Text := 'show columns FROM system where Field = ''SchemaNumber''';
    try
      qSchemaNumber.Execute;
    except
      on E: Exception do
      begin
        MessageDlg(E.Message , mtError, [mbOk], 0);
        exit;
      end;
    end;

    if qSchemaNumber.RecordCount > 0 then
    begin
      qSchemaNumber.SQL.Text := 'select SchemaNumber from system';
      try
        qSchemaNumber.Execute;
      except
        on E: Exception do
        begin
          MessageDlg(E.Message , mtError, [mbOk], 0);
          exit;
        end;
      end;

      if qSchemaNumber.RecordCount > 0 then
      begin
        qSchemaNumber.First;
        CurSchemaNumber := qSchemaNumber.FieldByName('SchemaNumber').AsInteger;
      end
      else
      begin
        MessageDlg('DB corrupted : Empty system table' , mtError, [mbOk], 0);
        exit;
      end;
    end
    else
    begin
      if IsConnecedOnlyCurrentUserToDB(AConnection) then
      begin
        qSchemaNumber.SQL.Text := 'ALTER TABLE system ADD COLUMN SchemaNumber int(10) DEFAULT 0';
        try
          qSchemaNumber.ExecSQL;
        except
          on E: Exception do
          begin
            MessageDlg(E.Message , mtError, [mbOk], 0);
            exit;
          end;
        end;

        CurSchemaNumber := 0;
      end;
    end;

     //move MysqlDump.exe
    {$IFNDEF VirtualUI}
    if (not FileExists(DM1.ProgrammDir + '\mysqldump.exe')) and (FileExists(ProgramFilesPath + '\' + C_MySQL_AllsystemsMax + '\bin\mysqldump.exe')) then
       CopyFile(PChar(ProgramFilesPath + '\' + C_MySQL_AllsystemsMax + '\bin\mysqldump.exe'), PChar(DM1.ProgrammDir + '\mysqldump.exe'), false);
    {$ENDIF}

    if CurSchemaNumber < DBSchemaNumber then
    begin
      if (MessageDlg(C_UPDATE_TEXT_MESSAGE, mtWarning, [mbYes, mbNo], 0) = mrYes) and
         IsConnecedOnlyCurrentUserToDB(AConnection)
      then
      begin
        CurForm.Show;
        CurForm.ActiveCursorHourGlass := True;
        Application.ProcessMessages;
        {$IFNDEF VirtualUI}
        CurForm.Label2.Caption := 'Creating database backup...';
        FrmLoadForm.Update;
        try
          TfrmBackup.Backup;
        except
        end;
        {$ENDIF}

        CurForm.Label2.Caption := 'Updating DATABASE to version ' + IntToStr(DBSchemaNumber) + '...';
        FrmLoadForm.Update;
        UpdateSchemaNumber := CurSchemaNumber;
        try
          if CurSchemaNumber < 1 then   // update 1
          begin
            if not RunUpdate1(AConnection) then
              exit;

            UpdateSchemaNumber := 1;
          end;

          if CurSchemaNumber < 2 then   // update 2
          begin
            if not RunUpdate2(AConnection) then
              exit;

            UpdateSchemaNumber := 2;
          end;

          if CurSchemaNumber < 3 then   // update 3
          begin
            if not RunUpdate3(AConnection) then
              exit;

            UpdateSchemaNumber := 3;
          end;

          if CurSchemaNumber < 4 then   // update 4
          begin
            if not RunUpdate4(AConnection) then
              exit;

            UpdateSchemaNumber := 4;
          end;

          if CurSchemaNumber < 5 then   // update 5
          begin
            if not RunUpdate5(AConnection) then
              exit;

            UpdateSchemaNumber := 5;
          end;

          if CurSchemaNumber < 6 then   // update 6
          begin
            if not RunUpdate6(AConnection) then
              exit;

            UpdateSchemaNumber := 6;
          end;

          if CurSchemaNumber < 7 then   // update 7
          begin
            if not RunUpdate7(AConnection) then
              exit;

            UpdateSchemaNumber := 7;
          end;

          if CurSchemaNumber < 8 then   // update 8
          begin
            if not RunUpdate8(AConnection) then
              exit;

            UpdateSchemaNumber := 8;
          end;

          if CurSchemaNumber < 9 then   // update 9
          begin
            if not RunUpdate9(AConnection) then
              exit;

            UpdateSchemaNumber := 9;
          end;

          if CurSchemaNumber < 10 then   // update 10
          begin
            if not RunUpdate10(AConnection) then
              exit;

            UpdateSchemaNumber := 10;
          end;

          if CurSchemaNumber < 11 then // update 11
          begin
            if not RunUpdate11(AConnection) then
               exit;

            UpdateSchemaNumber := 11;
          end;

          if CurSchemaNumber < 12 then // update 12
          begin
            if not RunUpdate12(AConnection) then
               exit;

            UpdateSchemaNumber := 12;
          end;

          if CurSchemaNumber < 13 then // update 13
          begin
            if not RunUpdate13(AConnection) then
               exit;

            UpdateSchemaNumber := 13;
          end;

          if CurSchemaNumber < 14 then // update 14
          begin
            if not RunUpdate14(AConnection) then
               exit;

            UpdateSchemaNumber := 14;
          end;

          if CurSchemaNumber < 15 then // update 15
          begin
            if not RunUpdate15(AConnection) then
               exit;

            UpdateSchemaNumber := 15;
          end;

          if CurSchemaNumber < 16 then // update 16
          begin
            if not RunUpdate16(AConnection) then
               exit;

            UpdateSchemaNumber := 16;
          end;

          if CurSchemaNumber < 17 then // update 17
          begin
            if not RunUpdate17(AConnection) then
               exit;

            UpdateSchemaNumber := 17;
          end;

          if CurSchemaNumber < 18 then // update 18
          begin
            if not RunUpdate18(AConnection) then
               exit;

            UpdateSchemaNumber := 18;
          end;

          if CurSchemaNumber < 19 then // update 19
          begin
            if not RunUpdate19(AConnection) then
               exit;

            UpdateSchemaNumber := 19;
          end;

          if CurSchemaNumber < 20 then // update 20
          begin
            if not RunUpdate20(AConnection) then
               exit;

            UpdateSchemaNumber := 20;
          end;

          if CurSchemaNumber < 21 then // update 21
          begin
            if not RunUpdate21(AConnection) then
               exit;

            UpdateSchemaNumber := 21;
          end;

          if CurSchemaNumber < 22 then // update 22
          begin
            if not RunUpdate22(AConnection) then
               exit;

            UpdateSchemaNumber := 22;
          end;

          if CurSchemaNumber < 23 then // update 23
          begin
            if not RunUpdate23(AConnection) then
               exit;

            UpdateSchemaNumber := 23;
          end;

          if CurSchemaNumber < 24 then // update 24
          begin
            if not RunUpdate24(AConnection) then
               exit;

            UpdateSchemaNumber := 24;
          end;

          if CurSchemaNumber < 25 then // update 25
          begin
            if not RunUpdate25(AConnection) then
               exit;

            UpdateSchemaNumber := 25;
          end;

          if CurSchemaNumber < 26 then // update 26
          begin
            if not RunUpdate26(AConnection) then
               exit;

            UpdateSchemaNumber := 26;
          end;

          if CurSchemaNumber < 27 then // update 27
          begin
            if not RunUpdate27(AConnection) then
               exit;

            UpdateSchemaNumber := 27;
          end;

          if CurSchemaNumber < 28 then // update 28
          begin
            if not RunUpdate28(AConnection) then
               exit;

            UpdateSchemaNumber := 28;
          end;

          if CurSchemaNumber < 29 then // update 29
          begin
            if not RunUpdate29(AConnection) then
               exit;

            UpdateSchemaNumber := 29;
          end;

          if CurSchemaNumber < 30 then // update 29
          begin
            if not RunUpdate30(AConnection) then
               exit;

            UpdateSchemaNumber := 30;
          end;

          if CurSchemaNumber < 31 then // update 31
          begin
            if not RunUpdate31(AConnection) then
               exit;

            UpdateSchemaNumber := 31;
          end;

          if CurSchemaNumber < 32 then // update 32
          begin
            if not RunUpdate32(AConnection) then
               exit;

            UpdateSchemaNumber := 32;
          end;

          if CurSchemaNumber < 33 then // update 33
          begin
            if not RunUpdate33(AConnection) then
               exit;

            UpdateSchemaNumber := 33;
          end;

          if CurSchemaNumber < 34 then // update 34
          begin
            if not RunUpdate34(AConnection) then
               exit;

            UpdateSchemaNumber := 34;
          end;

          // good end
          Result := true;
        finally
          CurForm.ActiveCursorHourGlass := False;
          Application.ProcessMessages;
          // update SchemaNumber
          if UpdateSchemaNumber <> CurSchemaNumber then
          begin
            qSchemaNumber.SQL.Text := 'update system SET SchemaNumber = ' + IntToStr(UpdateSchemaNumber);
            try
              qSchemaNumber.ExecSQL;
            except
              on E: Exception do
              begin
                MessageDlg(E.Message , mtError, [mbOk], 0);
              end;
            end;
          end;
        end;
      end;
    end
    else
      Result := true;
  finally
    FreeAndNil(qSchemaNumber);
  end;
end;

end.
