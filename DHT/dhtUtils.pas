{
 this file is part of Ares
 Aresgalaxy ( http://aresgalaxy.sourceforge.net )

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*****************************************************************
 The following delphi code is based on Emule (0.46.2.26) Kad's implementation http://emule.sourceforge.net
 and KadC library http://kadc.sourceforge.net/
*****************************************************************
 }

{
Description:
misc fuctions
}

unit dhtUtils;

interface

uses
 classes;

 function dht_packet_to_str(id:integer):string;

implementation

uses
 sysutils,windows,classes2,dhtconsts;

function dht_packet_to_str(id:integer):string;
begin
 case id of
  CMD_DHT_BOOTSTRAP_REQ:result:='BOOTSTRAP_REQ'; // send bootstrap nodes
  CMD_DHT_BOOTSTRAP_RES:result:='BOOTSTRAP_RES';

  CMD_DHT_HELLO_REQ:result:='HELLO_REQ'; //	 	        = $55; // ping pong
  CMD_DHT_HELLO_RES:result:='HELLO_RES'; //     	    = $56;

  CMD_DHT_REQID:result:='REQID'; //		   	        = $60; // find nodes
  CMD_DHT_RESID:result:='RESID'; //			          = $61;
  CMD_DHT_REQID2:result:='REQID2'; //              = $62;

  CMD_DHT_SEARCHKEY_REQ:result:='SEARCHKEY_REQ'; //		    = $70; // search and publish
  CMD_DHT_SEARCHKEY_RES:result:='SEARCHKEY_RES'; //		    = $71;
//CMD_DHT_PUBLISHKEY_REQ      = $72;
//CMD_DHT_PUBLISHKEY_RES	    = $73;
  CMD_DHT_PUBLISHKEY_REQ:result:='PUBLISHKEY_REQ'; //      = $75;
  CMD_DHT_PUBLISHKEY_RES:result:='PUBLISHKEY_RES'; //	    = $76;

  CMD_DHT_SEARCHHASH_REQ:result:='SEARCHHASH_REQ'; //		  = $80; // search and publish
  CMD_DHT_SEARCHHASH_RES:result:='SEARCHHASH_RES'; //		  = $81;
  CMD_DHT_PUBLISHHASH_REQ:result:='PUBLISHHASH_REQ'; //     = $82;
  CMD_DHT_PUBLISHHASH_RES:result:='PUBLISHHASH_RES'; //	    = $83;
  CMD_DHT_SEARCHPARTIALHASH_RES:result:='SEARCHPARTIALHASH_RES'; // = $84;

  CMD_DHT_IPREQ:result:='IPREQ'; //	              = $90;
  CMD_DHT_IPREP:result:='IPREP'; //	              = $91;
  CMD_DHT_CACHESREQ:result:='CACHESREQ'; //	          = $92;
  CMD_DHT_CACHESREP:result:='CACHESREP'; //	          = $93;
  CMD_DHT_FIREWALLCHECK:result:='FIREWALLCHECK'; //       = $95;
  CMD_DHT_FIREWALLCHECKINPROG:result:='FIREWALLCHECKINPROG'; // = $96;
  CMD_DHT_FIREWALLCHECKRESULT:result:='FIREWALLCHECKRESULT'; //$97;
else result:='Unknown '+inttostr(id);
end;

end;
end.